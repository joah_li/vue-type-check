# package.json 文件的配置讲解, 这里只针对 vscode 插件的特殊配置的讲解

## 如下标示使用 vscode 1.45.0 的 api

```json
"engines": {
  "vscode": "^1.45.0"
}
```

## 激活插件的配置，比如在 vue 文件中激活插件

```json
"activationEvents": [
  "onLanguage:vue"
]
```

## main: 插件的主入口文件(运行入口文件)

```json
"main": "./out/extension.js"
```

## contributes: 描述插件发布内容的对象， 这个对象中声明了插件的基本配置

- `contributes.configuration`

>在 configuration 中配置的内容会暴露给用户，用户可以从“用户设置”和“工作区设置”中修改你暴露的选项。configuration 是 JSON 格式的键值对，VS Code为用户提供了良好的设置支持, 你可以用`vscode.workspace.getConfiguration('myExtension')`读取配置值

```json
"contributes": {
  "configuration": {
    "type": "object",
    "title": "TypeScript configuration",
    "properties": {
      "typescript.useCodeSnippetsOnMethodSuggest": {
        "type": "boolean",
        "default": false,
        "description": "Complete functions with their parameter signature."
      },
      "typescript.tsdk": {
        "type": ["string", "null"],
        "default": null,
        "description": "Specifies the folder path containing the tsserver and lib*.d.ts files to use."
      }
    }
  }
}
```

- `contributes.configurationDefaults`

> 为特定的语言配置编辑器的默认值，修改这个配置会覆盖编辑器已经为语言提供的默认配置。下面的示例是修改markdown语言的默认配置。

```json
"contributes": {
    "configurationDefaults": {
        "[markdown]": {
            "editor.wordWrap": "on",
            "editor.quickSuggestions": false
        }
    }
}
```

- `contributes.commands`

> 设置命令标题和命令体，随后这个命令会显示在命令面板中, 也可以加上category前缀，在命令面板中会以分类显示

- 注意：当调用命令时（通过组合键或者在命令面板中调用），VS Code会触发激活事件onCommand:${command}, 下面的示例是修改markdown语言的默认配置。

```json
"contributes": {
    "commands": [{
        "command": "extension.sayHello",
        "title": "Hello World",
        "category": "Hello"
    }],
  "menus": {
   "commandPalette": [
    {
     "command": "vuetypecheck.hello",
     "when": "editorHasSelection"
    }
   ]
  }
}
```

- 配置了 menus 中的命令控制面板，当 editor 中产生一个选区时，会才会出现 extendsion.sayHello 命令
![avatar](./image/1.png)

- `contributes.keybindings`

- `contributes.languages`

  - `aliases` 属性包含着这门语言的可读性名称。这个列表的第一项会作为语言标签（在VS Code右下角状态栏显示）。

  - ![avatar](./image/2.png)

  - `configuration` 属性确定了语言配置文件的路径。路径是指相对插件文件夹的路径，通常是`./language-configuration.json`

#### 其中可以配置的字段是

    - comments：定义注释

      1. 单行注释(被'Toggle Block Comment'使用) // blockComment
      2. 用于标识行注释的起始token; (被'Add Line Comment'使用) // lineComment

    - brackets：定义括号

    - autoClosingPairs： 为自动闭合某个功能， 开符号输入后，编辑器会自动插入闭符号。使用`notIn`参数，关闭字符串或者注释中的符号对

    - surroundingPairs： 定义选中文本的开闭符号

    - folding：  定义编辑器中的代码应何时、应怎么样折叠
      1. offSide， 和一下个缩进块之间的代码块尾部的空行（用于基于缩进的语言，如Python or F#）
      2. markers， 使用正则自定义代码中的折叠区域标识符

    - wordPattern： 使用正则匹配编程语言中 哪些词 应该是单个词，

```json
"contributes": {
    "languages": [{
        "id": "vue",
        "extensions": [ ".vue" ],
        "aliases": [ "Vue", "fuckVue" ],
        "filenames": [ ... ],
        "firstLine": "^#!/.*\\bpython[0-9.-]*\\b",
        "configuration": "./language-configuration.json"
    }]
}
```

- `contributes.debuggers`： 配置VS Code的调试器

- `contributes.grammars`： 为一门语言配置TextMate语法， 必须提供应用语法的language，TextMate的scopeName确定了语法和文件路径

> 注意：包含语法的文件必须是JSON（以.json结尾的文件）或者XML的plist格式文件。

- `contributes.snippets`: 为语言添加代码片段。language属性必须是语言标识符而path则必须是使用VS Code代码片段格式的代码片段文件的相对路径

```json
"contributes": {
    "snippets": [{
        "language": "go",
        "path": "./snippets/go.json"
    }]
}
```

- `contributes.views`: 为VS Code 添加视图。你需要为视图指定唯一标识和名称。

  * explorer: 活动栏中的资源管理视图容器。
  * scm: 活动栏中的源代码管理(SCM) 视图容器。
  * debug: 活动栏中的调试视图容器。
  * test: 活动栏中的测试视图容器。

> 当用户打开视图，VS Code会触发onView:${viewId}激活事件, （比如：下面示例中的onView:nodeDependencies）。你也可以用when控制视图的可见性。

```json
"contributes": {
    "views": {
        "explorer": [
            {
                "id": "nodeDependencies",
                "name": "Node Dependencies",
                "when": "workspaceHasPackageJSON"
            }
        ]
    }
}
```

- activationEvents: 激活事件数组。在VS Code中，插件都是懒加载的, 你得为VS Code提供插件激活的时机

- `activationEvents.onLanguage`: onLanguage:${language}

  - 特定语言文件打开时激活; onLanguage只支持语言标识符中的值。

  - 在 activationEvents 数组中声明多个 onLanguage 入口实现多语言支持

```json
"activationEvents": [
    "onLanguage:python"
]
```

```json
"activationEvents": [
    "onLanguage:json",
    "onLanguage:markdown",
    "onLanguage:typescript"
]
```

- `activationEvents.onCommand`: 当调用命令时激活事件

```json
"activationEvents": [
    "onCommand:extension.sayHello"
]
```

- `activationEvents.workspaceContains`: 文件夹打开后，且文件夹中至少包含一个符合glob模式的文件时触发。

```json
"activationEvents": [
    "workspaceContains:**/.editorconfig"
]
```

- `activationEvents.onFileSystem`: 从协议（scheme）打开的文件或文件夹打开时触发。通常是file-协议，也可以用自定义的文件供应器函数替换掉，比如ftp、ssh。

```json
"activationEvents": [
    "onFileSystem:sftp"
]
```

- `activationEvents.onView`: 指定的视图id展开时触发

```json
"activationEvents": [
    "onView:nodeDependencies"
]
```

- `activationEvents.onUri`: 插件的系统级URI打开时触发。这个URI协议需要带上vscode 或者 vscode-insiders协议。URI授权必须是插件的唯一标识，剩余的URI是可选的

```json
"activationEvents": [
    "onUri"
]
```

> 如果 vscode.git插件定义了onUri激活事件，那么下列任意URI打开时就会触发：

- `vscode://vscode.git/init`
- `vscode://vscode.git/clone?url=https%3A%2F%2Fgithub.com%2FMicrosoft%2Fvscode-vsce.git`
- `vscode-insiders://vscode.git/init (for VS Code Insiders)`

- `activationEvents.*`: 只在你的插件没有其他任何激活事件 激活事件 用于激活插件的VSCode事件钩子。的前提下，添加这个激活事件

```json
"activationEvents": [
    "*"
]
```

> 一个插件如果侦听了多个激活事件, 激活事件,用于激活插件的VSCode事件钩子。那么最好用"*"替换掉。
> 注意： 插件必须从它的主模块中输出一个`activate()`函数，当任意的激活事件触发时，VS Code会仅仅调用一次这个函数。此外，插件也应该 导出一个`deactivate()`函数，当VS Code关闭时执行清理的任务。如果`清理进程是异步`的，插件的`deactivate()`必须返回一个`Promise`。如果这个清理任务是同步的，那么`deactivate()`可以返回`undefined`
