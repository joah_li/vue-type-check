
type LogType = 'primary' | 'info' | 'warn' | 'debug' | 'success' | 'default' | 'danger';
class Logger {

  /**
   * info
   */
  public info(target: any) {
    this.print(target, 'info');
  }

  /**
   * warn
   */
  public warn(target: any) {
    this.print(target, 'warn');
  }

  /**
   * error
   */
  public error(target: any) {
    this.print(target, 'danger');
  }

  /**
   * 打印方法
   * @param text 输出文本
   * @param type 输出样式，可以是6个状态值，也可以是自定义颜色
   * @param back 是否将色值应用于背景色
   */
  private print(text: any, type: LogType = 'default', back = false) {
    text = `[插件 vue-type-check]: ${text}`;
    if (typeof text === 'object') { // 如果是对象则调用打印对象方式
      console.dir(text);
      return;
    }
    if (back) { // 如果是打印带背景图的
      console.log(
        `%c ${text} `,
        `background:${this.typeColor(type)}; padding: 2px; border-radius: 4px;color: #fff;`
      );
    } else {
      console.log(
        `%c ${text} `,
        `color: ${this.typeColor(type)};`
      );
    }
  };

  /**
   * @description 返回这个样式的颜色值
   * @param {String} type 样式名称 [ primary | success | warning | danger | info ]
   */
  private typeColor(type: LogType) {
    let color = '';
    switch (type) {
      case 'primary':
        color = '#2d8cf0';
        break;
      case 'success':
        color = '#19be6b';
        break;
      case 'info':
        color = '#909399';
        break;
      case 'warn':
        color = '#ff9900';
        break;
      case 'danger':
        color = '#f03f14';
        break;
      case 'default':
        color = '#35495E';
        break;
      default:
        color = type;
        break;
    }
    return color;
  }

}
const logger = new Logger();
export default logger;