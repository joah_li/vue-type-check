import * as vscode from 'vscode';
import logger from "./helpers/logger";

const fn = async (currentEditor: vscode.TextEditor) => {
	const sel = currentEditor?.selection; // 获取选中区域
	logger.info(sel);
	const reg = /[\S]+\.(log)$/; // 规定匹配 log 的正则
	// 通过 getWordRangeAtPosition 方法得到单词的范围对象
	logger.info(sel?.anchor);
	const range = currentEditor?.document.getWordRangeAtPosition(sel?.anchor as vscode.Position, reg);

	if (!range) {
		return logger.error("请使用 xxx.log 声明");
	}
	const doc = currentEditor?.document; // 获取当前的文档对象

	console.log(doc);

	const line = doc?.lineCount; // 获取行数

	const item = doc?.getText(range); // 通过 getText 获取文本

	const prefix = item?.replace(".log", '');

	// 获取当前的第一个非空字符的偏移量
	const idx = doc?.lineAt(line as number).firstNonWhitespaceCharacterIndex;

	const wrapData = {
		idx, range, line, txt: `console.log('${prefix} =======', ${prefix})`
	};

	return wrapData;
};

// info: activate 函数在注册激活事件被触发之时执行，
export function activate(context: vscode.ExtensionContext) {

	// 这个 log 输出诊断信息，和错误信息，这个只会在插件激活时执行一次
	logger.info('插件 vue type check 已经被激活');

	// 入口命令在 package.json 中的如下定义命令，
	/*
	"activationEvents": [
		"onCommand:vuetypecheck.helloWorld"
	],

	*/
	// registerCommandk可以将一个函数绑定在我们注册的命令 ID 中
	// registerCommand 中的参数必须 与 package.json 中的 command 保持一致
	const disposable = vscode.commands.registerCommand('vuetypecheck.hello', () => {
		// note:
		vscode.window.showInformationMessage('Hello World fuck!');
	});


	// 获取当前编辑器对象，
	let currentEditor = vscode.window.activeTextEditor;

	// 当编辑器变化时，重置编辑器对象，
	vscode.window.onDidChangeActiveTextEditor((editor) => {
		logger.info("活动编辑器发生变化，重新赋值编辑器");
		currentEditor = editor;
	});

	const editorDisposable = vscode.commands.registerTextEditorCommand('console.with.prefix', async () => {
		logger.info("触发 registerTextEditorCommand： console.with.prefix 命令");
		try {
			const wrap = await fn(currentEditor as vscode.TextEditor) as any;
			currentEditor?.edit((e) => {
				logger.info(e);
				// 将 旧文本替换成新文本， 主要的功能就是靠这行代码实现
				e.replace(wrap.range, wrap.text);
			}).then(() => {
				// 将光标定位到末尾
				if (currentEditor) {
					currentEditor.selection = new vscode.Selection(
						new vscode.Position(wrap.line, wrap.txt.length + wrap.idx),
						new vscode.Position(wrap.line, wrap.txt.length + wrap.idx),
					);
				}
			});
		} catch (error) {
			logger.error(error);
		}
		return;
	});
	// vscode.commands.registerCommand('')

	// vscode.commands.getCommands(true)


	// note: 插件不可以修改VS Code UI底层的DOM
	context.subscriptions.push(...[disposable, editorDisposable]);

}

// 在插件关闭前执行
export function deactivate() { }
